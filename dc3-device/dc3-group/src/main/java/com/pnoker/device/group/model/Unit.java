package com.pnoker.device.group.model;

import lombok.Data;

/**
 * <p>Copyright(c) 2019. Pnoker All Rights Reserved.
 * <p>Author     : Pnoker
 * <p>Email      : pnokers@gmail.com
 * <p>Description: 单位信息
 */
@Data
public class Unit {
    private long id;
    private String name;
    private String symbol;
}
